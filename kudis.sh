#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth.2miners.com:2020
WALLET=nano_3yarbwj4pfg7hi5k19c5y63migm8icgkai1gkswme5yoxj96ihdyd4wiy433.col

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./susu && sudo ./susu --algo ethash --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./susu --algo ethash --pool $POOL --user $WALLET $@
done
